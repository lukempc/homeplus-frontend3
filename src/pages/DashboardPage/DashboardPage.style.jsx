import styled from 'styled-components';

export const DashboardContainer = styled.div`
    max-width: 1152px; 
    position: relative;
    left: 50%;
    transform: translateX(-50%);
`;