import { React, useEffect } from 'react';
import { useParams } from 'react-router';

import useTaskForm from '../../hooks/useTaskForm';
import LoadingPage from '../../components/LoadingPage';
import TaskDetailCard from '../../components/TaskDetailCard';

const TaskDetailPage = () => {
  const params = useParams();
  const { getTaskById, isError, isLoading, task } = useTaskForm();

  useEffect(() => {
    if (!task) getTaskById(params.id);
  }, [task, getTaskById, params.id]);
  return (
    <div>
      {!isLoading && !isError && task && <TaskDetailCard key={task.id} details={task} />}
      <div className="loadingAndError">
        {isLoading && <LoadingPage />}
        {isError && <h3>{isError}</h3>}
      </div>
    </div>
  );
};

export default TaskDetailPage;
