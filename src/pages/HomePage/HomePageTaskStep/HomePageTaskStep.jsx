import React from 'react';
import TaskStep from './Components/TaskStep';
import { Box, Container } from './HomePageTaskStep.style';

const HomePageTaskStep = () => (
  <Box>
    <Container>
      <TaskStep step="1" description="Post your tasks" />
      <TaskStep step="2" description="Receive offers from taskers" />
      <TaskStep step="3" description="Accept an offer" />
      <TaskStep step="4" description="Get your tasks done" />
    </Container>
  </Box>
);

export default HomePageTaskStep;
