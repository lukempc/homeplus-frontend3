import styled from 'styled-components';

export const Container = styled.div`
  top: 7vh;
  position: absolute;
  width: 100vw;
  max-height: 72vh;
  min-height: 93vh;
  overflow-x: hidden;
  font-family: roboto, sans-serif;
  left: 0;
  background-color: #f2f2f2;
  a {
    text-decoration: none;
  }
  a:-webkit-any-link {
    color: #444;
  }
`;
