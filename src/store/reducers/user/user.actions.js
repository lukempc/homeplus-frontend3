export const setUser = (userObj) => {
  return {
      type: "SET_USER",
      payload: userObj
  }
}

export const logOut = () => {
  return {
      type: "LOG_OUT"
  }
}

export const askLogin = () => {
  return {
      type: "ASK_LOGIN"
  }
}

export const turnOffLogin = () => {
  return {
      type: "TURN_OFF_LOGIN"
  }
}

export const setTasker = (taskerObj) => {
  return {
      type: "SET_TASKER",
      payload: taskerObj
  }
}

export const setFollowTasks = (followList) => {
  const followingTasks = followList.map(follow => follow.taskEntity);
  return {
      type: "SET_FOLLOW_TASKS",
      payload: followingTasks
  }
}

export const followTask = (taskObj) => {
  return {
    type: "FOLLOW_TASK",
    payload: taskObj
  }
}
