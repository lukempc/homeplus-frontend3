import _ from 'lodash';

const currentUser = (state = {
    user: {},
    tasker: {},
    loggedIn: false,
    askLogin: false,
    followedTasks: []
}, action) => {
    switch(action.type){
        case "SET_USER":
            return {
                ...state,
                user: action.payload,
                loggedIn: true
            }
        case "LOG_OUT":
            return {
                user: {},
                tasker: {},
                loggedIn: false,
                askLogin: false,
                followedTasks: []
            }
        case "ASK_LOGIN":
            return {
                ...state,
                askLogin: true
            }
        case "TURN_OFF_LOGIN":
            return {
                ...state,
                askLogin: false
            }
        case "SET_TASKER":
            return {
                ...state,
                tasker: action.payload
            }
        case "SET_FOLLOW_TASKS":
            return {
                ...state,
                followedTasks: action.payload
            }
        case "FOLLOW_TASK":
            let tasksList = [];
            if (!_.isEmpty(state.followedTasks)) tasksList = state.followedTasks.map(task => task.id) 

            if (tasksList.includes(action.payload.id)) {
                return {
                    ...state,
                    followedTasks: state.followedTasks.filter(task => !(task.id === action.payload.id) )
                }
            } else {
                if (!_.isEmpty(state.followedTasks)) {
                    return {
                        ...state,
                        followedTasks: [ ...state.followedTasks, action.payload ]
                    }
                } else {
                    return {
                        ...state,
                        followedTasks: [ action.payload ]
                    }
                }
            }
        default:
            return state
    }
}

export default currentUser;
