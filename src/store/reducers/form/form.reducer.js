import TaskFormActionTypes from './form.types';
import initialValues from '../../../components/TaskForm/utils/InitialFValues';

const INITIAL_STATE = initialValues;

const formReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TaskFormActionTypes.UPDATE_FIELD:
      return {
        ...state,
        [action.payload.name]: action.payload.value,
      };
    case TaskFormActionTypes.BACK_TO_INIT:
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default formReducer;
