import React from 'react';
import { Box, Container, Row, Column, Heading, Bottom } from './Footer.style.jsx';

const Footer = () => (
  <Box>
    <Container>
      <Row>
        <Column>
          <Heading href="#">About Us</Heading>
          <Heading href="#">How It Works</Heading>
        </Column>
        <Column>
          <Heading>Our location</Heading>
          <span>30 Victoria St., NSW 3000</span>
        </Column>
        <Column>
          <Heading>Contact Us</Heading>
          <span>+61 000 000 000</span>
        </Column>
        <Column>
          <Heading>
            {' '}
            <br />
          </Heading>
          <span>service@homeplus.com</span>
        </Column>
      </Row>
    </Container>
    <Bottom>Copyright&copy; HomePlus Pty. Ltd 2022-Present, All rights reserved</Bottom>
  </Box>
);
export default Footer;
