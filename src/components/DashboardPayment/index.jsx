import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/system';
import Typography from '@mui/material/Typography';
import { Buttonstyle } from './Payment.styled';

const Payment = ({ BSB_NUM, ACC_NUM }) => {
  return (
    <div>
      <Box
        sx={{
          mt: '20px',
          mr: '2px',
          display: 'flex',
          flexDirection: 'row',
          width: '95%',
          justifyContent: 'right',
          color: '#444',
        }}
      >
        <Buttonstyle variant="outlined">Edit</Buttonstyle>
      </Box>
      <Box
        sx={{
          ml: '150px',
          display: 'flex',
          justifyContent: 'left',
          color: '#444',
        }}
      >
        <dl>
          <br />
          <dt>
            <Typography variant="h5">Bank Account</Typography>
          </dt>
          <dd>
            <Typography sx={{ fontSize: '16' }}>
              <pre>
                BsB Number: {'   '}
                {BSB_NUM}
              </pre>
            </Typography>
            <Typography sx={{ fontSize: '16' }}>
              <pre>
                Account Number: {'   '}
                {ACC_NUM}
              </pre>
            </Typography>
          </dd>
        </dl>
      </Box>
    </div>
  );
};

Payment.propTypes = {
  BSB_NUM: PropTypes.string.isRequired,
  ACC_NUM: PropTypes.string.isRequired,
};

export default Payment;
