import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import CardContent from '@mui/material/CardContent';
// import CardHeader from '@mui/material/CardHeader';
import Typography from '@mui/material/Typography';
import stringAvatar from '../../../utils/avatar.util';

const Summery = ({tasker}) => {
  const user = tasker.userEntity;
  const avatar = {...stringAvatar(user.name).sx}
  const sx = {...avatar, width: 60, height: 60, marginTop: 1}
  return (
    <Box sx={{ display: 'flex', width: '90%'}}>
      <Avatar sx={sx}>{user.name[0]}</Avatar>
      <CardContent>
        <Typography>{user.name}</Typography>
        <Typography>{`${user.postcode}, ${user.state}`}</Typography>
      </CardContent>
    </Box>
  );
};

Summery.propTypes = {
  tasker: PropTypes.object.isRequired,
};

export default Summery;
