import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import formatDistance from 'date-fns/formatDistance';
import Avatar from '@mui/material/Avatar';
import { Card, Section, User, Message } from './CommentBox.style';
import stringAvatar from '../../utils/avatar.util';


const CommentBox = ({ comment }) => {
  const username = _.toString(comment.userEntity.name);
  return (
    <Card id={comment.id}>
      <User>
          <Avatar {...stringAvatar(username)} />
        <span>{username}</span>
      </User>
      <Section>
        <Message>{comment.message}</Message>
        <p className="timeStamp">{formatDistance(new Date(comment.created_time), new Date(), { addSuffix: true })}</p>
      </Section>
    </Card>
  );
};

CommentBox.propTypes = {
  comment: PropTypes.object.isRequired,
};

export default CommentBox;
