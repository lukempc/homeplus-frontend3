const SelectionValues = {
  timeSections: [
    ['Morning', 'Before 10:00'],
    ['Midday', '10:00 - 14:00'],
    ['Afternoon', '14:00 - 18:00'],
    ['Evening', 'After 18:00'],
  ],
  typesSelections: [
    ['Apartment', 'House'],
    ['A few items', 'Apartment', 'House'],
  ],
};

export default SelectionValues;
