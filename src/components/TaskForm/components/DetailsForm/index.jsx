import { React } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import Input from '@mui/material/Input';

import { updateField } from '../../../../store/reducers/form/form.actions';

import CleaningSelections from './CleaningSelections';
import RemovalSelections from './RemovalSelections';
import HandySelections from './HandySelections';

const DetailsForm = ({ values, isBudgetError, handyErrors, first2CateErrors, updateFields }) => {
  const handleChange = (type) => (e) => {
    updateFields(type, e.target.value);
  };

  return (
    <div>
      <h4>What is your budget?</h4>
      <FormControl sx={{ m: 1, width: '300px' }} variant="standard">
        <InputLabel htmlFor="standard-adornment-amount">Budget</InputLabel>
        <Input
          id="standard-adornment-amount"
          name="budget"
          type="number"
          value={values.budget}
          onChange={handleChange('budget')}
          required
          startAdornment={<InputAdornment position="start">$</InputAdornment>}
          error={isBudgetError && values.budget <= 0}
        />
      </FormControl>
      {values.category === 'cleaning' && <CleaningSelections first2CateErrors={first2CateErrors} />}
      {values.category === 'removal' && <RemovalSelections first2CateErrors={first2CateErrors} />}
      {values.category === 'handy' && <HandySelections handyErrors={handyErrors} />}
    </div>
  );
};

DetailsForm.propTypes = {
  values: PropTypes.object.isRequired,
  updateFields: PropTypes.func,
  isBudgetError: PropTypes.bool,
  handyErrors: PropTypes.bool,
  first2CateErrors: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsForm);
