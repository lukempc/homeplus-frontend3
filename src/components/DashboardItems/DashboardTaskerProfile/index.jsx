import React from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import {
  Wrapper,
  LineWrapper,
  Label,
  CategoryLabel,
  TextArea,
} from './DashboardTaskerProfile.style';
import Tag from '../../TaskStatusTag';

const TaskerDashboardProfile = () => {
  const { tasker } = useSelector(state => state.currentUser);
  return (
    <Wrapper>
      <LineWrapper>
        <Label>Job Title</Label>
      </LineWrapper>
      <TextArea>{_.upperFirst(tasker.title)}</TextArea>
      <LineWrapper>
        <Label>Introduction</Label>
      </LineWrapper>
      <TextArea>
        {tasker.introduction}
      </TextArea>
      <LineWrapper>
        <Label>Skill Description</Label>
      </LineWrapper>
      <TextArea>
        {tasker.skills_description}
      </TextArea>
      <LineWrapper>
        <CategoryLabel>Category</CategoryLabel>
      </LineWrapper>
      <TextArea>
        <Tag label={tasker.category} />
      </TextArea>
      <LineWrapper>
        <CategoryLabel>Certificattions</CategoryLabel>
      </LineWrapper>
      <LineWrapper>
      <TextArea>
        {tasker.certifications}
      </TextArea>
      </LineWrapper>
    </Wrapper>
  );
};

export default TaskerDashboardProfile;
