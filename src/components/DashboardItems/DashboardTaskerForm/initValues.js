export const initValues = {
    title: '',
    introduction: '',
    skills_description: '',
    certifications: '',
    category: 'Cleaning',
    bank_bsb: '',
    bank_account: ''
}