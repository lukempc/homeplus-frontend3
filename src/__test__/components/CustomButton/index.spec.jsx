import React from 'react';
import CustomButton, { AlertDot } from '../../../components/CustomButton';
import Button from '@mui/material/Button';
import { shallow } from 'enzyme';

describe('../../../components/CustomButton', () => {
  it('should render CustomButton by default', () => {
    const wrapper = shallow(<CustomButton>testing</CustomButton>);
    expect(wrapper.find(Button)).toHaveLength(1);
  });

  it('should have a children property with a value of testing', () => {
    const wrapper = shallow(<CustomButton>testing</CustomButton>);
    expect(wrapper.find(Button).props().children).toEqual([null, 'testing']);
  });

  it('should have a children property with a value of testing', () => {
    const wrapper = shallow(<CustomButton alertDot>testing</CustomButton>);
    expect(wrapper.find(AlertDot)).toHaveLength(1);
  });
});
