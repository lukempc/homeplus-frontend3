import { useState } from 'react';
import Geocode from 'react-geocode';

Geocode.setApiKey(process.env.REACT_APP_GOOGLE_MAPS_API_KEY);

const UseGeocode = () => {
  const [isLoading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [geoResults, setResults] = useState([]);
  const [latLng, setLatLng] = useState(null);

  const getTaskWithLatLng = async (task) => {
    const address = [task.street, task.suburb, task.postcode, task.state].filter(Boolean).join(', ');
    const { results } = await Geocode.fromAddress(address);
    const { lat, lng } = await results[0].geometry.location;

    return await { lat, lng, ...task };
  };

  const geoRequest = async (tasks) => {
    setLoading(true);
    setError(false);
    if (tasks.length !== 0) {
      const tasksWithLatLng = await Promise.all(tasks?.map(async (task) => await getTaskWithLatLng(task)));
      if (!tasksWithLatLng) {
        setLoading(false);
        setError('Something went wrong');
        return;
      }
      setLoading(false);
      setResults(tasksWithLatLng);
    } else {
      setLoading(false);
      setResults([]);
    }
  };

  const getLatLng = async (address) => {
    if (address === '') {
      return;
    } else {
      const { results } = await Geocode.fromAddress(address);
      const { lat, lng } = await results[0].geometry.location;

      setLatLng({ lat, lng });
    }
  };
  return {
    geoRequest,
    getLatLng,
    latLng,
    geoResults,
    isLoading,
    isError,
  };
};

export default UseGeocode;
